<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['home/send']    = 'home/send';
$route['(:any)/(:any)']    = 'home/index/$1/$2';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
