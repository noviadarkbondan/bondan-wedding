        <!-- start of hero -->
        <section class="hero-slider hero-style-1">
            <div class="swiper-container">
                <div class="slide-main-text">
                    <div class="container">
                        <div class="slide-title">
                            <h2>Dorian & Blit</h2>
                        </div>
                        <div class="wedding-date">
                            <span>14 Feb 2020</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="count-down-clock">
                            <div id="clock"></div>
                        </div>
                    </div>
                    <div class="pattern">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/slider/slide-1.jpg"></div> 
                    </div> 

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/slider/slide-2.jpg"></div>
                    </div> 

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/slider/slide-3.jpg"></div>
                    </div><!-- end swiper-slide --> 
                </div>
                <!-- end swiper-wrapper -->

                <!-- swipper controls -->
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
        <!-- end of hero slider -->


        <!-- start invitation-section -->
        <section class="invitation-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="invitation-box">
                            <div class="left-vec"></div>
                            <div class="right-vec"></div>
                            <div class="inner">
                                <h2>Save the Date</h2>
                                <span>For the wedding of</span>
                                <h3>Dorian & Blit</h3>
                                <p>Friday February 2020 At 6 O clock PM ,Hotle Seraton, Gulistan Bonani 9500, Dhaka Bangladesh</p>
                                <a href="index.html#rsvp" class="theme-btn" id="scroll">RSVP now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end invitation-section -->


        <!-- start couple-section -->
        <section class="couple-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Happy Couple</h2>
                            <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="couple-area">
                            <div class="couple-row clearfix">
                                <div class="img-holder">
                                    <img src="<?php echo base_url();?>assets/images/couple/img-1.jpg" alt>
                                </div>
                                <div class="details">
                                    <div class="inner">
                                        <h2>Dorina Wiliam</h2>
                                        <h4>Hi Dear, I am Dorian Wiliam.</h4>
                                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman and above it there hung a picture that he had recently cut out of an illustrated magazine And housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright</p>
                                        <ul class="social-links">
                                            <li><a href="index.html#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="couple-row last-row clearfix">
                                <div class="details">
                                    <div class="inner">
                                        <h2>Blit Jhon</h2>
                                        <h4>Hi Dear, I am Blit Jhon.</h4>
                                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman and above it there hung a picture that he had recently cut out of an illustrated magazine And housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright</p>
                                        <ul class="social-links">
                                            <li><a href="index.html#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="img-holder">
                                    <img src="<?php echo base_url();?>assets/images/couple/img-2.jpg" alt>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end couple-section -->


        <!-- start story-section -->
        <section class="story-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Our story</h2>
                            <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="story-block">
                            <ul>
                                <li>
                                    <div class="details">
                                        <h3>First meet</h3>
                                        <span class="date">25 Dec 2018</span>
                                        <p>Luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>
                                    </div>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-1.png" alt>
                                    </div>
                                </li>
                                <li>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-2.png" alt>
                                    </div>
                                    <div class="details">
                                        <h3>First date</h3>
                                        <span class="date">25 Dec 2018</span>
                                        <p>Luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="details">
                                        <h3>Proposal</h3>
                                        <span class="date">25 Dec 2018</span>
                                        <p>Luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>
                                    </div>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-3.png" alt>
                                    </div>
                                </li>
                                <li>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-4.png" alt>
                                    </div>
                                    <div class="details">
                                        <h3>Engagement</h3>
                                        <span class="date">25 Dec 2018</span>
                                        <p>Luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end story-section -->


        <!-- start gallery-section -->
        <section class="gallery-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Captured Moments</h2>
                            <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-xs-12">
                        <div class="gallery-container gallery-fancybox masonry-gallery">
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-1.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-1.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-2.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-2.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-3.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-3.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-4.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-4.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-5.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-5.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/gallery/img-7.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-7.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="https://www.youtube.com/embed/XSGBVzeBUbk?autoplay=1" data-type="iframe" class="video-play-btn">
                                    <img src="<?php echo base_url();?>assets/images/gallery/img-6.jpg" alt class="img img-responsive">
                                    <i class="ti-control-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end gallery-section -->


        <!-- start contact-section -->
        <section class="contact-section section-padding p-t-0" id="rsvp">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Are you attending?</h2>
                            <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="contact-form">
                            <form id="rsvp-form" class="form validate-rsvp-form row" method="post">
                                <div class="col col-sm-6">
                                    <input type="text" name="name" class="form-control" placeholder="Your Name*">
                                </div>
                                <div class="col col-sm-6">
                                    <input type="email" name="email" class="form-control" placeholder="Your Email*">
                                </div>
                                <div class="col col-sm-6">
                                    <select class="form-control" name="guest" >
                                        <option disabled selected>Number Of Guest*</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                                <div class="col col-sm-6">
                                    <select class="form-control" name="events" >
                                        <option disabled selected>I Am Attending*</option>
                                        <option>Al events</option>
                                        <option>Wedding ceremony</option>
                                        <option>Reception party</option>
                                    </select>
                                </div>
                                <div class="col col-sm-12">
                                    <textarea class="form-control" name="notes" placeholder="Your Message*"></textarea>
                                </div>
                                <div class="col col-sm-12 submit-btn">
                                    <button type="submit" class="theme-btn">Send Invitation</button>
                                    <div id="loader">
                                        <i class="ti-reload"></i>
                                    </div>
                                </div>
                                <div class="col col-md-12 success-error-message">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end contact-section -->


        <!-- start event-section -->
        <section class="event-section section-padding p-t-0">
            <div class="top-area">
                <h2>Celebrate Our Love</h2>
                <p class="date">14 Feb 2020</p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="event-grids clearfix">
                            <div class="grid">
                                <h3>The Reception</h3>
                                <p>13 February, 2020 at 9AM - 11 AM Hotel seraton, Gusal Dhank, Bangladesh</p>
                                <p class="phone">Ph: 0123456634654</p>
                                <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.672778464467!2d89.55846281543346!3d22.814585729793365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901c9481c799%3A0x92f932dd6112f8ff!2sHotel+Sheraton+Buliding%2C+29+Khan+Jahan+Ali+Rd%2C+Khulna!5e0!3m2!1sen!2sbd!4v1558933503904!5m2!1sen!2sbd" class="location popup-gmaps">See location</a>
                            </div>
                            <div class="grid">
                                <h3>The Reception</h3>
                                <p>13 February, 2020 at 9AM - 11 AM Hotel seraton, Gusal Dhank, Bangladesh</p>
                                <p class="phone">Ph: 0123456634654</p>
                                <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.672778464467!2d89.55846281543346!3d22.814585729793365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901c9481c799%3A0x92f932dd6112f8ff!2sHotel+Sheraton+Buliding%2C+29+Khan+Jahan+Ali+Rd%2C+Khulna!5e0!3m2!1sen!2sbd!4v1558933503904!5m2!1sen!2sbd" class="location popup-gmaps">See location</a>
                            </div>
                            <div class="grid">
                                <h3>The Reception</h3>
                                <p>13 February, 2020 at 9AM - 11 AM Hotel seraton, Gusal Dhank, Bangladesh</p>
                                <p class="phone">Ph: 0123456634654</p>
                                <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.672778464467!2d89.55846281543346!3d22.814585729793365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901c9481c799%3A0x92f932dd6112f8ff!2sHotel+Sheraton+Buliding%2C+29+Khan+Jahan+Ali+Rd%2C+Khulna!5e0!3m2!1sen!2sbd!4v1558933503904!5m2!1sen!2sbd" class="location popup-gmaps">See location</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end event-section -->


        <!-- start partners-section -->
        <section class="partners-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Gift Registry</h2>
                        <div class="partner-grids clearfix">
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-1.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-2.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-3.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-4.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-5.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-6.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-7.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php echo base_url();?>assets/images/partners/img-8.jpg" alt>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end partners-section -->


        <!-- start blog-section -->
        <section class="blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Latest form the blog</h2>
                            <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="blog-grids clearfix">
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?php echo base_url();?>assets/images/blog/img-1.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="index.html#">Hundreds of designs for every wedding style</a></h3>
                                    <p class="date"><i class="ti-timer"></i> April 14, 2019</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?php echo base_url();?>assets/images/blog/img-2.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="index.html#">How to plan a perfect wedding in moments</a></h3>
                                    <p class="date"><i class="ti-timer"></i> April 14, 2019</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="entry-media">
                                    <img src="<?php echo base_url();?>assets/images/blog/img-3.jpg" alt>
                                </div>
                                <div class="details">
                                    <h3><a href="index.html#">Possible no matter where you are on your journey</a></h3>
                                    <p class="date"><i class="ti-timer"></i> April 14, 2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end blog-section -->