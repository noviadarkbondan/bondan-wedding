        <!-- start contact-section -->
        <section class="contact-section section-padding p-t-0" id="rsvp">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Terimakasih</h2>
                            <p>Mohon agar selalu mengenakan masker dan mematuhi protokol kesehatan.</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end contact-section -->